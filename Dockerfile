FROM node:13.12-slim
WORKDIR /home/app

# --- run the application
CMD /bin/bash -c "npm install; npm start"
